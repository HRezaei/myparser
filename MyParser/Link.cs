﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyParser
{
    class Linker
    {
        public static void Link(String FName)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //            * startInfo.FileName = "lnk.bat";
            //startInfo.Arguments = /*"D:\\masm32\bin\\ml /c /Zd /coff " +*/ "" + "first" + "" /*+ ".asm"*/;

            startInfo.WorkingDirectory = "d:\\masm32\\bin\\";
            startInfo.FileName = "link";
            startInfo.Arguments = "  /SUBSYSTEM:CONSOLE " + "\"" + FName + ".obj\" /out:\"" + FName + ".exe\"";
            process.StartInfo = startInfo;
            process.Start();
            //string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
        }
    }
}
