﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyParser
{
    class Translate
    {
        Char TAB = '\t';
        Char CR = '\n';
        public const int AdressSize = 4;
        public const int BaseOffset = AdressSize + 4;//4 is for pushing ebp inside a call
        public enum OutputMode { RealTime, StandBy, Off };
        private OutputMode _mode = OutputMode.RealTime;
        private String Output;
        private String _buffer;
        public String Buffer
        {
            get
            {
                String tmp = _buffer;
                _buffer = "";
                return tmp;
            }
        }
        public int Lcount = 0;

        public Translate()
        {
            Output = "";
            Lcount = 0;
            FarsiNames = new Dictionary<String, String>();
        }

        public String GetText()
        {
            return Output;
        }

        public OutputMode Mode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
            }
        }


        Dictionary<String, String> FarsiNames = new Dictionary<String, String>();
        private String Finglish(String N)
        {
            String fin = "";
            Dictionary<Char, String> Alefba = new Dictionary<Char, String>() { 
            { 'ا', "a" }, { 'ب', "b" }, { 'پ', "p" }, { 'ت', "t" }, { 'ث', "ss" },
            { 'ج', "j" }, { 'چ', "ch" }, { 'ح', "h" }, { 'خ', "kh" },{ 'د', "d" },
            { 'ذ', "z" }, { 'ر', "r" }, { 'ز', "zz" }, { 'ژ', "zh" }, { 'س', "s" }, { 'ش', "sh" },
            { 'ص', "sad" }, { 'ض', "zad" }, { 'ط', "ta" }, { 'ظ', "za" }, { 'ع', "ayn" },
            { 'غ', "gha" }, { 'ف', "f" }, { 'ق', "gh" }, { 'ک', "k" }, { 'گ', "g" }
            , { 'ل', "l" }, { 'م', "m" }, { 'ن', "n" },{ 'و', "v" }, { 'ه', "hh" },
            { 'ی', "y"} , { 'ئ', "u" }};
            if (FarsiNames.ContainsKey(N))
            {
                return FarsiNames[N];
            }
            else
            {
                for (int i = 0; i < N.Length; i++)
                {
                    if (Alefba.ContainsKey(N[i]))
                        fin += Alefba[N[i]];
                    else
                        fin += N[i];
                }
                while (FarsiNames.ContainsValue(fin)) fin += Lcount++;
                FarsiNames[N] = fin;
            }
            return fin;
        }

        /*
        RichTextBox
        public Translator(RichTextBox a) { 
        
        }
         * */

        //{ Output a String with Tab }
        private void Emit(string s)
        {
            switch (Mode)
            {
                case OutputMode.RealTime:
                    Output += s;
                    break;
                case OutputMode.StandBy:
                    _buffer += s;
                    break;

            }

        }

        //{ Output a String with Tab and CRLF }
        private void EmitLn(string s)
        {
            //Emit(" ");
            switch (Mode)
            {
                case OutputMode.RealTime:
                    Output += (TAB + s + Char.ConvertFromUtf32(10));
                    break;
                case OutputMode.StandBy:
                    _buffer += (TAB + s + Char.ConvertFromUtf32(10));
                    break;

            }
        }

        public void DirectEmit(String s)
        {
            Output += s;
        }

        public String NewLabel(String Prefix, Boolean VeryNew = true)
        {
            if (VeryNew)
                return Prefix + (++Lcount).ToString();
            else
                return Prefix + (Lcount).ToString();
        }

        public void PostLabel(String l)
        {
            Emit(l + ": \n");
        }

        public void Clear()
        {
            EmitLn("MOV eax,0");
        }

        public void Negate()
        {
            EmitLn("NEG eax");
            //EmitLn("FCHS");
        }

        public void NotIt()
        {
            EmitLn(@"NOT eax
                AND eax,1");
        }

        public void LoadNumericConst(String n, DataType ConstType)
        {
            switch (ConstType)
            {
                case DataType.Integer:
                    EmitLn("MOV eax," + n);
                    break;
                case DataType.Real:
                    int m;
                    if (Int32.TryParse(n, out m))
                        EmitLn("MOVfp feax, FP8(" + n + ".0)");
                    else
                        EmitLn("MOVfp feax, FP8(" + n + ")");
                    break;
            }
            // 
        }
        /*
                public void LoadRealConst(String n)
                {
                    //EmitLn("MOV eax," + n);
                    EmitLn("FLD FP8(" + n + ")");
                }
                */
        public void LoadStringConst(String N)
        {
            if (Mode != OutputMode.Off)
            {
                String sum = "";
                //String res = "";
                Byte[] asciiBytes = Encoding.Unicode.GetBytes(N);
                int i = 0;
                /*
                 for (i = 0; i < asciiBytes.Length; i += 2)
                //foreach (byte b in asciiBytes)
                {
                    sum = asciiBytes[i + 1].ToString("X") + asciiBytes[i + 0].ToString("X");// +asciiBytes[i + 1] + asciiBytes[i + 2] + asciiBytes[i + 3];
                    EmitLn("mov [outtemp+" + i.ToString() + "]," + sum + "h");
                    sum = "";
                }
                EmitLn("MOV [outtemp+" + i.ToString() + @"],0h
                    LEA eax,outtemp");
                 */
                String name = NewLabel("StrConst");
                String Out = "";
                Out = name + TAB + "WORD ";
                for (i = 0; i < asciiBytes.Length; i += 2)
                {
                    if (asciiBytes[i] < 16)
                        sum = asciiBytes[i + 1].ToString("X") + "0" + asciiBytes[i].ToString("X");// +asciiBytes[i + 1] + asciiBytes[i + 2] + asciiBytes[i + 3];
                    else
                        sum = asciiBytes[i + 1].ToString("X") + asciiBytes[i].ToString("X");
                    Out += sum + "h,";
                    sum = "";
                }
                Out += "0h";
                Output = Output.Replace(";@@@", Out + "\n;@@@");

                EmitLn("LEA eax," + name);
            }
        }

        public void LoadBooleanConst(Boolean n)
        {
            if (n)
            {
                EmitLn("MOV eax,1");
            }
            else
            {
                EmitLn("MOV eax,0");
            }

        }

        public void LoadVar(String Name, DataType VarType, DataType AsType)
        {
            //if (!InTable(Name)) Undefined(Name);
            switch (AsType)
            {
                case DataType.Integer:
                case DataType.Boolean:
                    EmitLn("MOV eax," + Finglish(Name));
                    break;
                case DataType.Real:
                    if (VarType == DataType.Integer)
                    {
                        EmitLn("FILD Dword ptr [" + Finglish(Name) + @"]
                FSTP feax");
                    }
                    else
                        EmitLn("MOVfp feax," + Finglish(Name));
                    break;
                case DataType.String:
                    EmitLn("LEA eax," + Finglish(Name));
                    break;
            }
        }

        public void LoadParam(Parameter Param, DataType DType)
        {
            String AbsOffset = "";
            if (Param.Type != ParamType.Local)
                AbsOffset = (BaseOffset + Param.Offset).ToString();
            else
                AbsOffset = (Param.Offset).ToString();
            switch (DType)
            {
                case DataType.Integer:
                case DataType.Boolean:
                    EmitLn("MOV eax,[ebp+" + AbsOffset + "]");
                    if (Param.Type == ParamType.ByRef) Derefrence();
                    break;
                case DataType.Real:
                    /**
                     * @Todo
                     */
                    if (Param.Type == ParamType.ByRef)
                    {
                        EmitLn("MOV eax,[ebp+" + AbsOffset + @"]
                        MOVfp feax,QWord ptr [eax]");
                    }
                    else //if (Param.Type == ParamType.ByVal)
                    {
                        EmitLn("MOVfp feax,QWord ptr [ebp+" + AbsOffset + "]");
                    }
                    break;
                case DataType.String:
                    //if (PType == ParamType.ByRef)
                    //else if (PType == ParamType.ByVal)  
                    // there is no diffrence
                    EmitLn("MOV eax,[ebp+" + AbsOffset + "]");
                    break;
            }

        }

        public void Derefrence()
        {
            EmitLn("MOV eax,[eax]");
        }

        public void GetRefrence(String N)
        {
            EmitLn("LEA eax," + Finglish(N));
        }

        public void StoreParam(Parameter Param)
        {
            String AbsOffset = "";
            if (Param.Type != ParamType.Local)
                AbsOffset = (BaseOffset + Param.Offset).ToString();
            else
                AbsOffset = (Param.Offset).ToString();
            if (Param.Type == ParamType.ByRef)
            {

                EmitLn("MOV ebx,[ebp+" + AbsOffset + "]");
                switch (Param.DType)
                {
                    case DataType.Integer:
                    case DataType.Boolean:
                        EmitLn("MOV [ebx],eax ");
                        break;
                    case DataType.String:
                        EmitLn("invoke ucCopy,eax,ebx");
                        break;
                    case DataType.Real:
                        EmitLn("MOVfp qword ptr [ebx],feax ");
                        break;
                }

            }
            else
            {
                switch (Param.DType)
                {
                    case DataType.Integer:
                    case DataType.Boolean:
                        EmitLn("MOV [ebp+" + AbsOffset + "],eax ");
                        break;
                    case DataType.String:
                        EmitLn("invoke ucCopy,eax,[ebp+" + AbsOffset + "]");
                        break;
                    case DataType.Real:
                        EmitLn("MOVfp qword ptr [ebp+" + AbsOffset + "],feax ");
                        break;
                }
            }
        }

        public void Push()
        {
            EmitLn("PUSH eax");
        }

        public void FPush()
        {
            EmitLn("PUSHfp feax");
        }

        public void PopAdd(DataType Type)
        {
            switch (Type)
            {
                case DataType.Integer:
                    EmitLn(@"POP ebx
                ADD eax,ebx");
                    break;
                case DataType.Real:
                    EmitLn(@"POPfp febx
                ADDfp feax,febx");
                    break;
            }
            /**/
        }

        public void PopSub(DataType Type)
        {
            switch (Type)
            {
                case DataType.Integer:
                    EmitLn(@"POP ebx
                   SUB eax,ebx
                   NEG eax");
                    break;
                case DataType.Real:
                    EmitLn(@"POPfp febx
                SUBfp feax,febx");
                    break;
            }
            /**/
        }

        public void PopMul(DataType Type)
        {
            switch (Type)
            {
                case DataType.Integer:
                    EmitLn(@"POP ebx
                MUL ebx");
                    break;
                case DataType.Real:
                    EmitLn(@"POPfp febx
                MULfp feax,febx");
                    break;
            }

            /**/
        }

        public void PopDiv(DataType Type)
        {
            switch (Type)
            {
                case DataType.Integer:
                    EmitLn("MOV ebx,eax");
                    EmitLn("POP eax");
                    EmitLn("MOV edx,0");
                    EmitLn("DIV ebx");
                    break;
                case DataType.Real:
                    EmitLn(@"POPfp febx
                DIVfp feax,febx");
                    break;
            }
            /**/
        }

        public void PopAnd()
        {
            EmitLn(@"POP ebx
                AND eax,ebx");
        }

        public void PopOr()
        {
            EmitLn(@"POP ebx
                OR eax,ebx");
        }

        public void PopXor()
        {
            EmitLn(@"POP ebx
                XOR eax,ebx");
        }

        public void PopCompare(DataType Type)
        {
            switch (Type)
            {
                case DataType.Integer:
                case DataType.Boolean:
                    EmitLn(@"POP ebx
                CMP ebx,eax");
                    break;
                case DataType.Real:
                    EmitLn(@"POPfp febx
                CMPfp feax,febx");
                    break;
            }
        }

        public void PopCompareString()
        {
            /*EmitLn("POP ebx");
            EmitLn("CMP ebx,eax");*/
            EmitLn(@"POP ebx
                invoke ucCmp ,eax,ebx
                CMP eax,0");
        }

        public void PopAppend()
        {
            EmitLn(@"POP ebx
                PUSH eax                
                invoke ucCopy,ebx,addr [outtemp]
                POP eax
                invoke ucCatStr,addr [outtemp],eax
                LEA eax,outtemp");
        }

        public void SetEqual()
        {
            EmitLn(@"SETE al
                AND eax,1");
            //EmitLn("EXT eax");
        }

        public void SetNEqual()
        {
            EmitLn(@"SETNE al
                AND eax,1");
            //EmitLn("EXT eax");
        }

        public void SetGreater()
        {
            EmitLn(@"SETG al
                AND eax,1");
            //EmitLn("EXT eax");
        }

        public void SetLess()
        {
            EmitLn(@"SETL al
                and eax,1");
            //EmitLn("EXT eax");
        }

        public void SetLessOrEqual()
        {
            EmitLn(@"SETLE al
                AND eax,1");
            //EmitLn("EXT eax");
        }

        public void SetGreaterOrEqual()
        {
            //EmitLn("SLE eax");
            EmitLn(@"SETGE al
                AND eax,1");
        }

        public void StoreVar(String Name, DataType DType)
        {
            switch (DType)
            {
                case DataType.Integer:
                    EmitLn("MOV [" + Finglish(Name) + "],eax");
                    break;
                case DataType.Real:
                    EmitLn("MOVfp [" + Finglish(Name) + "], feax");
                    break;
                case DataType.String:
                    EmitLn("invoke ucCopy,eax,addr [" + Finglish(Name) + "]");
                    break;
                case DataType.Boolean:
                    EmitLn("MOV [" + Finglish(Name) + "],eax");
                    //EmitLn("Storing a boolean variable is not implemented yet!");
                    break;
            }


        }

        public void Branch(String L)
        {
            EmitLn("JMP " + L);

        }

        public void BranchFalse(String L)
        {
            //EmitLn(".if eax");
            EmitLn(@"CMP ax,0
                JZ " + L);
        }

        public void ReadIt(String Msg, DataType DType)
        {
            String and = "";
            if (DType == DataType.Boolean)
            {
                Msg += "(0=نادرست و 1=درست)";
                and = "AND eax,1";
            }
            LoadStringConst(Msg + ": ? ");
            WriteString();
            switch (DType)
            {
                case DataType.Integer:
                case DataType.Boolean:
                    EmitLn(@"invoke StdIn, addr outtemp,255
                invoke atol, addr outtemp
                "+and);
                    break;
                case DataType.String:
                    EmitLn(@"invoke StdInW, addr outtemp,255
                        LEA eax,outtemp");
                    break;
                case DataType.Real:
                    EmitLn(@"invoke StdIn, addr outtemp,255
                            invoke StrToFloat, addr outtemp,addr feax");
                    break;

            }
            //Store(name);
        }

        public void WriteInteger()
        {
            EmitLn("print str$(eax)");
        }

        public void WriteReal()
        {
            EmitLn("print real8$(feax)");
            /*EmitLn("invoke FloatToStr,feax,addr outtemp");
               EmitLn("print addr outtemp");*/
        }

        public void WriteString()
        {
            EmitLn("invoke StdOutW, eax");
        }

        public void WriteBoolean()
        {
            EmitLn("PrintBool eax");
        }

        public void Header()
        {
            EmitLn(@".486                   
    .model flat, stdcall                    
    option casemap :none                    
 
    include d:\masm32\include\windows.inc     
    include d:\masm32\include\masm32.inc
    
    include d:\masm32\include\gdi32.inc
    include d:\masm32\include\user32.inc
    include d:\masm32\include\kernel32.inc
    include d:\masm32\include\msvcrt.inc

    include d:\masm32\macros\macros.asm       
    
    include d:\masm32\include\Fpu.inc
    
    include g:\hosein\programming\assembly\myfplib.inc

    includelib \masm32\lib\masm32.lib

    includelib \masm32\lib\gdi32.lib
    includelib \masm32\lib\user32.lib
    includelib \masm32\lib\kernel32.lib
    includelib \masm32\lib\msvcrt.lib
    .data
        hasadl DWORD 0
        feax QWORD 0
        febx QWORD 0
    ;@@@
    outtemp WORD 255 DUP(?)
    StrTrue	WORD 62Fh,631h,633h,62Ah,0h
	StrFalse WORD 646h,627h,62Fh,631h,633h,62Ah,0h
        .code
        
PrintBool Macro reg
    LOCAL El,En
	cmp reg,0
	je El
	invoke StdOutW, addr StrTrue
	jmp En
	El:invoke StdOutW, addr StrFalse
	En:
EndM
");

        }

        public void Prolog()
        {
            PostLabel("start");
        }
        
        public void Epilog()
        {
            EmitLn("print chr$(10)");
            LoadStringConst("جهت بستن برنامه کلیدی را بزنید");
            WriteString();
            EmitLn(@"MOV eax,input(""..."")
                invoke ExitProcess,eax
end start");
        }

        public void Call(String N)
        {
            EmitLn("CALL " + Finglish(N));
        }

        public void CleanStack(int n)
        {
            if (n > 0)
            {
                EmitLn("ADD esp," + n.ToString());
            }
        }

        //{ Wr i te the Prolog f o r a Procedure }
        public void ProcProlog(String N)
        {
            //PostLabel(N);
            Emit(Finglish(N) + ":\n");
            EmitLn(@"PUSH ebp
                MOV ebp,esp");
        }

        //{ Wr i te the Epi log f o r a Procedure }
        public void ProcEpilog(String N, int ParamsSize)
        {
            EmitLn(@"MOV esp,ebp
                POP ebp");
            EmitLn("RET " + ParamsSize.ToString());
            Emit(";" + Finglish(N) + " endp \n");
        }

        //{ Allocate Storage for a Static Variable }
        public void Allocate(String Name, DataType Dtype, String Val)
        {
            if (Mode != OutputMode.Off)
                switch (Dtype)
                {
                    case DataType.Integer:
                    case DataType.Boolean:
                        Output = Output.Replace(";@@@", Finglish(Name) + " " + TAB + "DWORD " + Val + "\n;@@@");
                        break;
                    case DataType.Real:
                        Output = Output.Replace(";@@@", Finglish(Name) + " " + TAB + "QWORD " + Val + "\n;@@@");
                        break;
                    case DataType.String:
                        if (Val != "0")
                            Output = Output.Replace(";@@@", ";@@@\n" + Finglish(Name) + " " + TAB + "WORD " + Val);
                        else
                            Output = Output.Replace(";@@@", ";@@@\n" + Finglish(Name) + " " + TAB + "WORD 255 DUP(?)");
                        break;


                }
            //EmitLn(Finglish(Name) + " : " + TAB + "DC " + Val);
        }

        public void ForCompare(String counter, String end, String NegStep, String Continu, Parser p)
        {
            EmitLn(@"MOV ebx,[esp+4]
                CMP eax,0
                JL " + NegStep);
            /*
             * Optimizition issue:
             * Next instruction depends on type of counter,
             * If it is a global variable we can simply emit(CMP ebx,[GlobalVarName])
             * If it is a parameter(passed with value) or local varibale we can emit(CMP ebx,[ebp+ParamOffset])
             * And if it is a refrence to caller variables,we should emit diffrently
             * I choosed a solution like below just for readability and it is not optimized neither in assembley code,nor in our parser!
             */
            p.Load(counter, DataType.Integer);
            EmitLn(@"CMP ebx,eax
                JL " + end + @"
                JMP " + Continu + @"
                " + NegStep + " :");
            p.Load(counter, DataType.Integer);
            EmitLn(@"CMP ebx,eax
                JG " + end);
        }
        /*
        //public void ForCompare(Parser.Parameter param, String end, String NegStep, String Continu)
        public void ForCompare(int counterOffset, String end, String NegStep, String Continu)
        {
            EmitLn(@"MOV ebx,[esp+4]
                CMP eax,0
                JL " + NegStep);
            LoadParam(counterOffset);
            EmitLn(@"CMP ebx,eax
                JL " + end + @"
                JMP " + Continu);
                PostLabel(NegStep);
            LoadParam(counterOffset);
            EmitLn(@"CMP ebx,eax
                JG " + end);
        }
        */
        public void ForNext(String var, String next, Parser p)
        {
            EmitLn("POP ebx");
            /*
             * Optimizition issue:
             * Next load and store are depended on type of var,
             * See Optimizition issue in "public void ForCompare" procedure
             * I choosed a solution like below just for readability and it is not optimized neither in assembley code,nor in our parser!
             */

            p.Load(var, DataType.Integer);
            EmitLn("ADD eax,ebx");
            p.Store(var);
            EmitLn("JMP " + next);
        }

        public void ForNext(int counterOffset, String next)
        {
            EmitLn("POP ebx");
            EmitLn("ADD [ebp+" + counterOffset.ToString() + "],ebx");
            EmitLn("JMP " + next);
        }

        public void AllocateRoom(int room)
        {
            EmitLn("SUB esp," + room.ToString());
        }

        public static int GetDataTypeSize(DataType DType)
        {
            int res = 4;
            switch (DType)
            {
                case DataType.Integer:
                case DataType.String:
                case DataType.Boolean:
                    res = 4;
                    break;
                case DataType.Real:
                    res = 8;
                    break;
            }
            return res;
        }

        public void PassStringByVal()
        {
            String PName = NewLabel("StrParam");
            Allocate(PName, DataType.String, "0");
            EmitLn("invoke ucCopy,eax,addr [" + PName + @"]
                LEA eax," + PName + @"
                PUSH eax");

        }

        public void LocalStrParam(int offset)
        {
            String PName = NewLabel("LocStr");
            Allocate(PName, DataType.String, "0");
            EmitLn("LEA eax,[" + PName + @"]
                MOV [ebp+" + offset.ToString() + "],eax");
        }

        /*
                public void tmpAddParam(String Name) {
                    EmitLn("LOCAL " + Finglish(Name) + ":QWORD");
                }

                public void LoadParamValues(String[] Params,int count) {
                    for (int i = 0; i < count + 2; i++) EmitLn("pop eax");
                    for (int i = count-1; i >= 0; i--) EmitLn("pop " + Finglish(Params[i]));
                }
         * */
        /*
                public Translate Clone() {
                    return (Translate)this.MemberwiseClone();
                }
                */

    }
}
