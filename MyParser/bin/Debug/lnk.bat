@echo off

if exist %1.exe del %1.exe

if not exist rsrc.obj goto nores

d:\masm32\bin\Link /SUBSYSTEM:WINDOWS /OUT:%1.exe /LIBPATH:"d:\masm32\lib\" /OPT:NOREF %1.obj rsrc.obj
dir %1.*
goto TheEnd

:nores
d:\masm32\bin\Link /SUBSYSTEM:WINDOWS /OUT:%1.exe /LIBPATH:"d:\masm32\lib\" /OPT:NOREF %1.obj
dir %1.*

:TheEnd

if errorlevel 0 dir %1.* > lnk.txt

d:\masm32\tview.exe lnk.txt