@echo off

if exist %2 del %2

D:\masm32\bin\ml /c /coff /Fo %2 %1 > asmbl.txt

if errorlevel 0 dir %1.* >> asmbl.txt

D:\masm32\tview.exe asmbl.txt
