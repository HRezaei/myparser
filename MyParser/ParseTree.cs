﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyParser
{
    class ParseTree
    {
        public System.Windows.Forms.TreeNode CurNode=new System.Windows.Forms.TreeNode("به نام خدا");
        public enum TreeMode { On, Off };
        private TreeMode _mode;
        public TreeMode Mode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
            }
        }

        /*public ParseTree() { 
        
        }*/
        
        public void AddNode(String N)
        {
            if(Mode==TreeMode.On)CurNode = CurNode.Nodes.Add(N);

        }

        public void AddParent(String N)
        {
            if (Mode == TreeMode.On) if (CurNode.Nodes.Count > 0)
            {
                System.Windows.Forms.TreeNode tmp = CurNode.Nodes[CurNode.Nodes.Count - 1];
                CurNode.Nodes[CurNode.Nodes.Count - 1].Remove();
                CurNode = CurNode.Nodes.Add(N);
                CurNode.Nodes.Add(tmp);
            }
            else
            {
                CurNode = CurNode.Nodes.Add(N);
            }
            //tmp. = CurNode;
        }

        public void Up()
        {
            if (Mode == TreeMode.On) CurNode = CurNode.Parent;

        }
    }
}
