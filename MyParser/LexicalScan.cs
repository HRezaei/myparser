﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyParser
{
    //Lexical scanner
    class LexicalScan
    {
        String Input;
        Parser Parser;
        public Char Look;
        public char Token;
        public String Value;
        public int pos = 0;
        public int line = 1, col = 0;

        static Char TAB = '\t';
        static Char CR = '\n';

        /**
         * Are there all of Keywords struct members in the array below?
         * Are all of them necessary?
         * */
        String[] KWList = { Keywords.If, Keywords.Else, Keywords.EndIf, Keywords.While, Keywords.EndWhile, Keywords.Read, Keywords.Write, Keywords.Var, Keywords.End, Keywords.Procedure, Keywords.EndProc, Keywords.Program, Keywords.For, Keywords.Step, Keywords.Input, Keywords.InOut, Keywords.Local,Keywords.VarType,Keywords.True,Keywords.False };
        String KWcode = "xileweRWvePeBfsIOLT^^";


        public LexicalScan(String Source, Parser Parent)
        {
            Input = Source;
            Parser = Parent;
            line = pos = col = 0;
            GetChar();
            Next();

        }

        //{ Read New Character From Input Stream }
        void GetChar()
        {
            if (pos < Input.Length)
            {
                Look = Input[pos];
                pos++;
                if (Look == CR) { line++; col = 0; } else col++;

            }
            else
            {
                Look = '@';
            }
        }

        //{ Get an Identifier }
        public void GetName()
        {
            SkipWhite();
            if (!IsAlpha(Look)) Parser.Expected("شناسه", "\"" + Look.ToString() + "\"");
            Value = "";
            Token = Tokens.Identifier;
            do
            {
                Value = Value + char.ToUpper(Look);
                GetChar();
            } while (IsAlNum(Look) || Look == '_');
            /*
             * we can check the value here for a legal type, and if it is,set token to 'T' or 't' for example,that
             * represent that we have a type identifier
             * this approach does not allow user to use some strings like "صحیح" ,"رشته",... anywhere else than variable declrations or string constants.
             * */
        }

        //{ Get a Number }
        void GetNum()
        {
            SkipWhite();
            if (!IsDigit(Look)) Parser.Expected("عدد صحیح", "\"" + Look.ToString() + "\"");
            Token = Tokens.IntegerConst;
            Value = "";
            do
            {
                Value = Value + Look;
                GetChar();
            } while (IsDigit(Look));
            if (Look == Symbols.FPSign) GetReal();
        }

        void GetReal()
        {
            Token = Tokens.RealConst;
            Look = '.';
            do
            {
                Value = Value + Look;
                GetChar();
            } while (IsDigit(Look));
        }

        //{ Get a String }
        void GetString()
        {
            SkipWhite();
            if (Look!='«') Parser.Expected("ثابت رشته ای", "\""+Look.ToString()+"\"");
            GetChar();
            Token = Tokens.StringConst;
            Value = "";
            do
            {
                Value = Value + Look;
                GetChar();
            } while (Look!='»');
            GetChar();
        }
         

        //{ Get an Operator: this is my own and incompelete version }
        void GetOp()
        {
            SkipWhite();
            Value = Look.ToString();
            //Token = Tokens.Operator;
            Token = Look;
            GetChar();
        }

       public DataType GetDataType()
        {
           Next();
           DataType res = DataType.Integer;
           switch (Value)
            {
                case "صحیح":
                    res = DataType.Integer;
                break;
                case "حقیقی":
                    res = DataType.Real;
                break;
                case "رشته":
                    res = DataType.String;
                break;
                case "منطقی":
                    res = DataType.Boolean;
                break;
                default:
                Parser.Expected(" یک نوع تعریف شده ", "\"" + Value + "\"");
                    break;
            }
           Next(); 
           return res ;


        }

       public Boolean GetBooleanConst() {
           switch (Value)
           {
               case Keywords.True:
                   return true;
               case Keywords.False:
                   return false;
               default:
                   Parser.Expected("ثابت منطقی", "\"" + Value + "\"");
                   return false;
           }
       }

        public void Next()
        {
            SkipWhite();
            if (IsAlpha(Look))
                GetName();
            else if (IsDigit(Look))
                GetNum();
            else if (Look == '«')
                GetString();
            else
                GetOp();
            Scan();
        }

        void Scan()
        {
            /*
             * checks for a keyword,if current value is a keyword,sets token equal to its proper char
             * else sets token equal to 'x' that represents we have a name,identifier,...
             * */

            if (Token == Tokens.Identifier)
                /*if (Value == Symbols.ListSeparator.ToString()) 
                    Token = Symbols.ListSeparator;
                else*/
                    Token = KWcode[Parser.Lookup(KWList, Value) + 1];

        }

        //{ Recognize an Alpha Character }
        public static bool IsAlpha(char c)
        {
            return char.IsLetter(c);
        }

        //{ Recognize a Decimal Digit }
        public static bool IsDigit(char c)
        {
            return char.IsDigit(c);
        }

        public static bool IsAlNum(char c)
        {
            return IsAlpha(c) || IsDigit(c);
        }

        public static bool IsAddop(char c)
        {
            Char[] a = { Symbols.AddOp, Symbols.SubtractOp };
            return (Array.IndexOf(a, c) >= 0);
        }

        public static bool IsMulop(char c)
        {
            Char[] a = { Symbols.MultiplyOp, Symbols.DivideOp };
            return (Array.IndexOf(a, c) >= 0);
        }

        public static bool IsOrop(char c)
        {
            Char[] a = { '|', '~' };
            return (Array.IndexOf(a, c) >= 0);
        }

        public static bool IsRelop(Char c)
        {
            Char[] a = { '=', '#', '<', '>' };
            return (Array.IndexOf(a, c) >= 0);
        }

        public static bool IsWhite(char c)
        {
            Char[] a = { TAB, ' ', '\0', CR, Symbols.OpenComments };
            return (Array.IndexOf(a, c) >= 0);
        }

        void SkipWhite()
        {
            while (IsWhite(Look))
            {
                if (Look == Symbols.OpenComments)
                    SkipComment();
                else
                    GetChar();
            }
        }

        void SkipComment()
        {
            while (Look != Symbols.CloseComments)
            {
                GetChar();
                if (Look == Symbols.OpenComments) SkipComment();
            }
            GetChar();
        }

        public String GetRemainedCode()
        {
            return Value+" "+Look+this.Input.Substring(pos);
        }
        
        public LexicalScan Clone() {
            return (LexicalScan)this.MemberwiseClone();
        }
    }
}
