﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyParser
{
    class Cradle
    {
        //{ Constant Declarations }
        char TAB = '\t';

        //{ Variable Declarations }
        //Nullable<Char> Look;             // { Lookahead Character }
        Char Look;
        int pos = 0;

        //{ Read New Character From Input Stream }
        void GetChar()
        {
            if (pos < richTextBoxCode.Text.Length)
            {
                Look = richTextBoxCode.Text[pos];
                pos++;
            }
            else
            {
                Look = '@';
            }
        }

        //{ Report an Error }
        void Error(string s)
        {
            richTextBoxMsg.AppendText("\n");
            richTextBoxMsg.AppendText("خطا: " + s + ".");
        }

        //{ Report Error and Halt }
        void Abort(string s)
        {
            Error(s);
            throw new Exception("error: " + s);
        }

        //{ Report What Was Expected }
        void Expected(string s)
        {
            Abort(s + " نیاز است");
        }

        //{ Match a Specific Input Character }
        void Match(char x)
        {
            if (Look != x)
                Expected("'" + x + "'");
            else
            {
                GetChar();
                //SkipWhite();
            }
        }

        //{ Recognize an Alpha Character }
        bool IsAlpha(char c)
        {
            return char.IsLetter(c);
        }


        //{ Recognize a Decimal Digit }
        bool IsDigit(char c)
        {
            return char.IsDigit(c);
        }

        bool IsAlNum(char c)
        {
            return IsAlpha(c) || IsDigit(c);
        }

        bool IsAddop(char c)
        {
            Char[] a = { '+', '-' };
            return (Array.IndexOf(a, c) >= 0);
        }

        bool IsWhite(char c)
        {
            return char.IsWhiteSpace(c);
        }

        void SkipWhite()
        {
            while (IsWhite(Look)) GetChar();
        }
        //{ Get an Identifier }
        Char GetName()
        {
            Char Token ;
            if (!IsAlpha(Look)) Expected("شناسه");

            /*while (IsAlNum(Look))
            {
                Token = Token + char.ToUpper(Look);
                GetChar();
            }
            SkipWhite();
            */
            Token = char.ToUpper(Look);
            GetChar();
            return Token;
        }

        //{ Get a Number }
        Char GetNum()
        {

            if (!IsDigit(Look)) Expected("عدد صحیح");
            /*while (IsDigit(Look))
            {
                Value = Value + Look;
                GetChar();
            }
            SkipWhite();*/

            return Look;
        }

        //{ Output a String with Tab }
        void Emit(string s)
        {
            richTextBoxResult.AppendText(TAB + s);
        }

        //{ Output a String with Tab and CRLF }
        void EmitLn(string s)
        {
            Emit(s);
            richTextBoxResult.AppendText("\n");
        }

        //{ Initialize }
        void Init()
        {
            pos = 0;
            richTextBoxMsg.Text = richTextBoxResult.Text = "";
            GetChar();
            SkipWhite();
        }




    }
}
