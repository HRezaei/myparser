﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyParser
{

    class Parser
    {

        private Translate Translator = new Translate();
        private LexicalScan Scanner;
        //private Form1 Interface;
        public String Errors;
        //public System.Windows.Forms.TreeNode Tree = new System.Windows.Forms.TreeNode("به نام خدا");
        //private System.Windows.Forms.TreeNode CurNode = new System.Windows.Forms.TreeNode();
        public ParseTree Tree = new ParseTree();
        //we can use dictionary structure instead of these 2 array's:
        String[] ST;
        Char[] SType;
        DataType[] DTypes;

        int ParamSizes = 0;
        int LocalSizes = 0;



        Dictionary<String, Parameter> Params = new Dictionary<String, Parameter>();
        Dictionary<String, KeyValuePair<string, Parameter>[]> Procedures = new Dictionary<String, KeyValuePair<string, Parameter>[]>();
        //{ Variable Declarations }
        int NEntry = 0;
        static Char CR = '\n';


        public Parser(String Source)
        {
            //Translator = new Translate();
            //Interface = Face;
            Scanner = new LexicalScan(Source, this);
            ST = new String[100];
            SType = new Char[100];
            DTypes = new DataType[100];
            Params.Clear();
            //any class property which has a initial value in the its declaration,doesn't need to initialise here:
            //NEntry = 0;
            AddEntry("حاصل", SymbolType.Var);
            Translator.Allocate("حاصل", DataType.Integer, "0");
            //Params.
            //NParam = 0;
            //CurNode = Tree;
        }

        //{ Report an Error }
        private void Error(string s)
        {
            Errors += "\n";
            Errors += "خطا: " + s + ".";
        }

        //{ Report Error and Halt }
        void Abort(string s)
        {
            Error(s);
            throw new Exception("خطا: " + s);
        }

        //{ Report What Was Expected }
        public void Expected(string s, String v)
        {
            Abort(s + " نیاز است اما " + v + " یافت شد");
        }

        //{ Report an undefined identifier }
        void Undefined(string s)
        {
            Abort(s + " تعریف نشده ");
        }

        void Duplicate(string s)
        {
            Abort(s + " قبلا تعریف شده ");
        }

        void CheckIdent()
        {
            /*
             * this shows an unfamiliar message because of token possible values are e,l,w,R,W,...
             * */
            if (Scanner.Token != Tokens.Identifier) Expected("شناسه", Farsi.Translate(Scanner.Token.ToString()));
        }

        //{ Table Lookup }
        //{ If the input string matches a table entry, return the entry
        //index.  If not, return a zero.  }
        public static int Lookup(String[] T, string s/*, int n*/)
        {
            return Array.IndexOf(T, s);
        }

        //{ Table Lookup }
        //{ If the input string matches a table entry, return the entry
        //index.  If not, return a zero.  }
        public static int Lookup(Char[] T, Char s/*, int n*/)
        {
            return Array.IndexOf(T, s);
        }

        int Locate(String N)
        {
            return Lookup(ST, N);
        }

        Char TypeOf(String N)
        {
            if (IsParam(N))
                return SymbolType.FormalParam;
            else
                return SType[Lookup(ST, N)];
        }

        DataType DataTypeOf(String N)
        {
            if (IsParam(N))
                return Params[N].DType;
            else
                return DTypes[Lookup(ST, N)];
        }

        bool InTable(String n)
        {
            return Lookup(ST, n) != -1;
        }

        /**
         * I rename this method from CheckTable to CheckTables
         * and modify it to search Params too
         * */
        Char CheckTables(String n)
        {
            if (InTable(n)) return SymbolType.Var;
            if (IsParam(n)) return SymbolType.FormalParam;
            if (Procedures.ContainsKey(n)) return SymbolType.Procedure;
            Undefined(n);
            return ' ';
        }

        void CheckDup(String n)
        {
            if (InTable(n)) Duplicate(n);
        }

        void AddEntry(String N, Char T, DataType DType = DataType.Integer)
        {
            CheckDup(N);
            //if (NEntry == MaxEntry) Abort("جدول نشانه ها پر است");
            ST[NEntry] = N;
            SType[NEntry] = T;
            DTypes[NEntry] = DType;
            NEntry++;
        }

        void MatchString(String x)
        {
            if (Scanner.Value != x) Expected('"' + x + '"', "\""+Scanner.Value+"\"");
            Scanner.Next();
        }

        void Semi()
        {
            if (Scanner.Token == ';') Scanner.Next();
        }

        void LoadVar(String Name, DataType VarType)
        {
            if (!InTable(Name)) Undefined(Name);
            //EmitLn("MOV eax,);
            Translator.LoadVar(Name, DataTypeOf(Name), VarType);
        }

        public void Load(String N, DataType VarType)
        {
            if (IsParam(N))
                LoadParam(N, VarType);
            else
                LoadVar(N, VarType);
        }

        public void Store(String Name)
        {
            if (IsParam(Name))
                StoreParam(Name);
            else
                StoreVar(Name);
        }

        void StoreVar(String Name)
        {
            if (!InTable(Name)) Undefined(Name);
            Translator.StoreVar(Name, DataTypeOf(Name));
            //EmitLn("MOV eax,(A0)");
        }

        /*
        void ReadIt(String name)
        {
            Translator.ReadIt("vared konid",DataType.Integer);
            Store(name);
        }
        */
        //{ Parse and Translate a Math Factor }
        void Factor(DataType Type)
        {
            if (Scanner.Token == Symbols.OpenParenthesis)
            {
                Scanner.Next();
                MathExpression(Type);
                MatchString(Symbols.CloseParenthesis.ToString());
            }
            else
            {
                if (Scanner.Token == Tokens.Identifier)
                {
                    //Ident();
                    CheckTables(Scanner.Value);
                    DataType FactorType = DataTypeOf(Scanner.Value);
                    if (!AreCompatible(FactorType, Type))
                        Expected("شناسه از نوع " + Farsi.Translate(Type.ToString()), Farsi.Translate(FactorType.ToString()));
                    Load(Scanner.Value, Type);
                    Tree.AddNode("شناسه " + Scanner.Value);
                    Tree.Up();
                }
                else if ((Scanner.Token == Tokens.IntegerConst) && ((Type == DataType.Integer) || (Type == DataType.Real)))
                {
                    Translator.LoadNumericConst(Scanner.Value, Type);
                    Tree.AddNode("عدد صحیح " + Scanner.Value);
                    Tree.Up();
                }
                else if ((Scanner.Token == Tokens.RealConst) && (Type == DataType.Real))
                {
                    Translator.LoadNumericConst(Scanner.Value, Type);
                    Tree.AddNode("عدد حقیقی " + Scanner.Value);
                    Tree.Up();
                }
                /*else if ((Scanner.Token == Tokens.StringConst) && (Type == DataType.String))
                {
                    Translator.LoadStringConst(Scanner.Value);
                }*/
                else
                    /*
                     * token has not a meaningfull value
                     * */
                    Expected("ثابت یا متغیر از نوع " + Farsi.Translate(Type.ToString()), Farsi.Translate(Scanner.Token.ToString()));
                Scanner.Next();
            }

        }

        //{ Recognize and Translate a Multiply }
        void Multiply(DataType Type)
        {
            Tree.AddParent("ضرب");
            Scanner.Next();
            Factor(Type);
            Translator.PopMul(Type);
            Tree.Up();
        }

        //{ Recognize and Translate a Divide }
        void Divide(DataType Type)
        {
            Tree.AddParent("تقسیم");
            Scanner.Next();
            Factor(Type);
            Translator.PopDiv(Type);
            Tree.Up();
        }

        //{ Parse and Translate a Math Term }
        void Term(DataType Type)
        {
            Factor(Type);
            while (LexicalScan.IsMulop(Scanner.Token))
            {
                /*
                 * We can Pass Type to Translator.Push() method and make this decision in it
                 * */
                if (Type == DataType.Integer)
                    Translator.Push();
                else if (Type == DataType.Real)
                    Translator.FPush();

                switch (Scanner.Token)
                {
                    case Symbols.MultiplyOp:
                        Multiply(Type);
                        break;
                    case Symbols.DivideOp:
                        Divide(Type);
                        break;
                }
            }
        }

        //{ Recognize and Translate an Add }
        void Add(DataType Type)
        {
            Tree.AddParent("جمع");
            Scanner.Next();
            Term(Type);
            Translator.PopAdd(Type);
            Tree.Up();
        }

        //{ Recognize and Translate a Subtract }
        void Subtract(DataType Type)
        {
            Tree.AddParent("تفریق");
            Scanner.Next();
            Term(Type);
            Translator.PopSub(Type);
            Tree.Up();
        }

        //{ Parse and Translate an MathExpression }
        void MathExpression(DataType Type)
        {
            //Tree.AddNode("عبارت");

            if (LexicalScan.IsAddop(Scanner.Token))
            {
                Translator.Clear();
                //Tree.AddNode("");
                //Tree.Up();
            }
            else
                Term(Type);

            while (LexicalScan.IsAddop(Scanner.Token))
            {
                /*
                 * We can Pass Type to Translator.Push() method and make this decision in it
                 * */
                if (Type == DataType.Integer)
                    Translator.Push();
                else if (Type == DataType.Real)
                    Translator.FPush();
                switch (Scanner.Token)
                {
                    case Symbols.AddOp:
                        Add(Type);
                        break;
                    case Symbols.SubtractOp:
                        Subtract(Type);
                        break;
                }
            }
            //Interface.Up();

        }

        //{ Parse and Translate an String Expression }
        void StringExpression()
        {
            StringTerm();
            while (LexicalScan.IsAddop(Scanner.Token))
            {
                /*
                 * We can Pass Type to Translator.Push() method and make this decision in it
                 * */

                Translator.Push();
                StringAdd();
            }
        }

        void StringTerm()
        {
            if (Scanner.Token == Symbols.OpenParenthesis)
            {
                Scanner.Next();
                StringExpression();
                MatchString(Symbols.CloseParenthesis.ToString());
            }
            else
            {
                if (Scanner.Token == Tokens.Identifier)
                {
                    //Ident();
                    CheckTables(Scanner.Value);
                    DataType FactorType = DataTypeOf(Scanner.Value);
                    if (!AreCompatible(FactorType, DataType.String))
                        Expected("شناسه از نوع رشته ای ", Farsi.Translate(FactorType.ToString()));
                    Load(Scanner.Value, DataType.String);
                    Tree.AddNode("شناسه " + Scanner.Value);
                    Tree.Up();
                }
                else if (Scanner.Token == Tokens.StringConst)
                {
                    Translator.LoadStringConst(Scanner.Value);
                    Tree.AddNode("ثابت رشته ای " + Scanner.Value);
                    Tree.Up();
                }
                else
                    /*
                     * token has not a meaningfull value
                     * */
                    Expected("ثابت یا متغیر از نوع رشته ای ", Farsi.Translate(Scanner.Token.ToString()));
                Scanner.Next();
            }

        }

        void StringAdd()
        {
            Tree.AddParent("الحاق");
            Scanner.Next();
            StringTerm();
            Translator.PopAppend();
            Tree.Up();
        }

        void Expression(DataType DType)
        {
            switch (DType)
            {
                case DataType.Boolean:
                    BoolExpression();
                    break;
                case DataType.Integer:
                case DataType.Real:
                    MathExpression(DType);
                    break;
                case DataType.String:
                    StringExpression();
                    break;

            }
        }

        void CompareExpression(DataType Dtype)
        {
            Expression(Dtype);
            Translator.PopCompare(Dtype);
        }

        void NextExpression(DataType Dtype)
        {
            Scanner.Next();
            CompareExpression(Dtype);
        }

        //{ Recognize and Translate a Relational "Equals" }
        void Equal(DataType Dtype)
        {
            NextExpression(Dtype);
            //PopCompare();
            Translator.SetEqual();
        }

        //{ Recognize and Translate a Relational "Equals" on strings }
        void EqualString()
        {
            Scanner.Next();
            StringExpression();
            Translator.PopCompareString();
            Translator.SetGreater();
        }
        //{ Recognize and Translate a Relational "Less Than or Equal" }
        void LessOrEqual(DataType Dtype)
        {
            NextExpression(Dtype);
            //PopCompare();
            Translator.SetLessOrEqual();
        }
        //{ Recognize and Translate a Relational "Not Equals" }
        void NotEqual(DataType Dtype)
        {
            NextExpression(Dtype);
            //PopCompare();
            Translator.SetNEqual();
        }

        //{ Recognize and Translate a Relational "Less Than" }
        void Less(DataType Dtype)
        {
            Scanner.Next();
            switch (Scanner.Token)
            {
                //implementing <= operator :
                case '=':
                    LessOrEqual(Dtype);
                    break;
                //implementing <> operator :
                case '>':
                    NotEqual(Dtype);
                    break;
                //implementing < operator :
                default:
                    CompareExpression(Dtype);
                    //PopCompare();
                    Translator.SetLess();
                    break;
            }
        }

        //{ Recognize and Translate a Relational "Greater Than" }
        void Greater(DataType Dtype)
        {
            Scanner.Next();
            //implementing >= operator :
            if (Scanner.Token == '=')
            {
                NextExpression(Dtype);
                //PopCompare();
                Translator.SetGreaterOrEqual();
            }
            else
            {
                CompareExpression(Dtype);
                //PopCompare();
                Translator.SetGreater();
            }
        }
        void RelationInteger()
        {

            MathExpression(DataType.Integer);
            if (LexicalScan.IsRelop(Scanner.Token))
            {
                Translator.Push();
                switch (Scanner.Token)
                {
                    case Symbols.EqualSign:
                        Equal(DataType.Integer);
                        break;
                    case '<':
                        Less(DataType.Integer);
                        break;
                    case '>':
                        Greater(DataType.Integer);
                        break;
                    //other relational operators:

                }

            }
            else
            {
                Expected("عملگر رابطه ای", Scanner.Token.ToString());
            }
        }

        void RelationReal()
        {

            MathExpression(DataType.Real);
            if (LexicalScan.IsRelop(Scanner.Token))
            {
                Translator.FPush();
                switch (Scanner.Token)
                {
                    case Symbols.EqualSign:
                        Equal(DataType.Real);
                        break;
                    case '<':
                        Less(DataType.Real);
                        break;
                    case '>':
                        Greater(DataType.Real);
                        break;
                    //other relational operators:

                }

            }
            else
            {
                Expected("عملگر رابطه ای", Scanner.Token.ToString());
            }
        }

        void RelationString()
        {
            StringExpression();
            if (LexicalScan.IsRelop(Scanner.Token))
            {
                Translator.Push();
                switch (Scanner.Token)
                {
                    case Symbols.EqualSign:
                        EqualString();
                        break;
                }
            }
            else
            {
                Expected("عملگر رابطه ای", Scanner.Token.ToString());
            }
        }

        void RelationBoolean()
        {
            if (Scanner.Token == Tokens.Identifier)
            {
                CheckTables(Scanner.Value);
                DataType FactorType = DataTypeOf(Scanner.Value);
                if (!AreCompatible(FactorType, DataType.Boolean))
                    Expected("شناسه از نوع منطقی ", Farsi.Translate(FactorType.ToString()));
                Load(Scanner.Value, DataType.Boolean);
                Tree.AddNode("شناسه " + Scanner.Value);
                Tree.Up();
                Scanner.Next();
            }
            else if (Scanner.Token == Tokens.BooleanConst)
            {
                Translator.LoadBooleanConst(Scanner.GetBooleanConst());
                Tree.AddNode("ثابت منطقی " + Scanner.Value);
                Tree.Up();
                Scanner.Next();
            }
            else {
                Expected("ثابت یا متغیر منطقی",Farsi.Translate(Scanner.Token.ToString()));
            }
        }

        //{ Parse and Translate a Relation }
        void Relation()
        {

            if (Scanner.Token == Symbols.OpenParenthesis)
            {
                Scanner.Next();
                //MathExpression(DataType.Real);
                BoolExpression();
                MatchString(Symbols.CloseParenthesis.ToString());
            }else
                {
                String[] Options = { "RelationBoolean", "RelationString", "RelationInteger", "RelationReal" };
                String Best = ParseBest(Options);
                /*switch (Best)
                {

                }*/
            }

        }

        void NotFactor()
        {
            if (Scanner.Token == Symbols.NegateOp)
            {
                Scanner.Next();
                BoolExpression();
                Translator.NotIt();
            }
            else
            {
                Relation();
            }
        }

        void BoolTerm()
        {
            //Tree.AddNode("جمله ی منطقی");

            NotFactor();
            while (Scanner.Token == Symbols.AndOp)
            {
                Translator.Push();
                Scanner.Next();
                NotFactor();
                Translator.PopAnd();
            }
            //Interface.Up();

        }

        void BoolOr()
        {
            Scanner.Next();
            BoolTerm();
            Translator.PopOr();
        }

        void BoolXor()
        {
            Scanner.Next();
            BoolTerm();
            Translator.PopXor();
        }

        void BoolExpression()
        {
            //Tree.AddNode("رابطه منطقی");
            BoolTerm();

            while (LexicalScan.IsOrop(Scanner.Token)|| Scanner.Token==Symbols.EqualSign)
            {
                Translator.Push();
                switch (Scanner.Token)
                {
                    case Symbols.OrOp:
                        BoolOr();
                        break;
                    case Symbols.XorOp:
                        BoolXor();
                        break;
                    case Symbols.EqualSign:
                        BoolXor();
                        Translator.NotIt();
                        break;
            

                }
            }
            //Interface.Up();

        }

        void Assignment()
        {
            /*
             * We don't need this checking here since it has been done in AssignOrProc method:
             */
            //CheckTables(Value);
            String Name = Scanner.Value;
            DataType Type = DataTypeOf(Name);
            Tree.AddNode("انتساب به " + Name);
            Scanner.Next();
            MatchString(Symbols.AssignOp.ToString());
            //String[] options = { "BoolExpression", "MathExpression(DataType.Real)", "MathExpression(DataType.Integer)" };
            //ParseBest(options);
            Expression(Type);
            Store(Name);
            Tree.Up();
        }

        void AssignOrProc()
        {
            CheckTables(Scanner.Value);
            //if (!(InTable(Value)||IsParam(Value))) Undefined(Value);
            switch (TypeOf(Scanner.Value))
            {
                case ' ':
                    Undefined(Scanner.Value);
                    break;
                case SymbolType.Var:
                case SymbolType.FormalParam:
                    Assignment();
                    break;
                case SymbolType.Procedure:
                    CallProc(Scanner.Value);
                    break;
                default:
                    Abort("شناسه ی " + Scanner.Value + " در اینجا قابل استفاده نیست");
                    break;

            }
        }

        void DoIf(/*String L*/)
        {
            Tree.AddNode(Keywords.If);
            Scanner.Next();
            Tree.AddNode("شرط منطقی");
            BoolExpression();
            Tree.Up();
            String L1 = Translator.NewLabel("EndIf");
            String L2 = L1;
            Translator.BranchFalse(L1);
            MatchString(Keywords.Bud);
            Tree.AddNode(Keywords.Bud);
            Block();
            Tree.Up();
            if (Scanner.Token == Tokens.Else)
            {
                Scanner.Next();
                L2 = Translator.NewLabel("EndElse");
                Translator.Branch(L2);
                Translator.PostLabel(L1);
                Tree.AddNode("نبود");
                Block();
                Tree.Up();
            }
            Translator.PostLabel(L2);
            MatchString(Keywords.EndIf);
            Tree.Up();
        }

        void DoWhile()
        {
            Tree.AddNode(Keywords.While);
            String L1, L2;
            Scanner.Next();
            L1 = Translator.NewLabel("WThen");
            L2 = Translator.NewLabel("EndWhile");
            //L3 = NewLabel();
            Translator.PostLabel(L1);
            BoolExpression();
            MatchString(Keywords.Hast);
            MatchString(Keywords.BeginBlock);
            Translator.BranchFalse(L2);
            Block();
            MatchString(Keywords.EndWhile);
            Translator.Branch(L1);
            Translator.PostLabel(L2);
            Tree.Up();
        }

        void DoFor()
        {

            //MatchString("برای");
            String BeginLabel, EndLabel, NegStepLabel, ContinueLabel, Counter;
            Scanner.Next();
            Counter = Scanner.Value;
            Tree.AddNode("حلقه برای " + Counter);
            Char CounterType = CheckTables(Counter);
            BeginLabel = Translator.NewLabel("For");
            EndLabel = Translator.NewLabel("EndFor", false);
            NegStepLabel = Translator.NewLabel("NegStep", false);
            ContinueLabel = Translator.NewLabel("Continue", false);
            Scanner.Next();
            MatchString(Keywords.Az);
            MathExpression(DataType.Integer);
            Store(Counter);
            MatchString(Keywords.Ta);
            MathExpression(DataType.Integer);
            Translator.Push();
            Translator.PostLabel(BeginLabel);
            //Scanner.Next();
            if (Scanner.Token == Tokens.Step)
            {
                MatchString(Keywords.Step);
                MathExpression(DataType.Integer);
            }
            else
            {
                Translator.LoadNumericConst("1", DataType.Integer);
            }
            Translator.Push();
            MatchString(Keywords.BeginBlock);
            //if (CounterType == 'v')
            Translator.ForCompare(Counter, EndLabel, NegStepLabel, ContinueLabel, this);
            //else
            //Translator.ForCompare(Params[Counter].Offset, EndLabel, NegStepLabel, ContinueLabel);
            Translator.PostLabel(ContinueLabel);
            Block();
            MatchString(Keywords.EndFor);
            //if (CounterType == 'v')
            Translator.ForNext(Counter, BeginLabel, this);
            //else
            //    Translator.ForNext(Params[Counter].Offset, BeginLabel);

            Translator.PostLabel(EndLabel);
            Translator.CleanStack(8);
            Tree.Up();
        }

        void ReadVar()
        {
            CheckIdent();
            CheckTables(Scanner.Value);
            Translator.ReadIt(Scanner.Value, DataTypeOf(Scanner.Value));
            Store(Scanner.Value);
            Scanner.Next();
        }

        //{ Process a Read Statement }
        void DoRead()
        {
            Scanner.Next();
            MatchString(Symbols.OpenParenthesis.ToString());
            ReadVar();
            while (Scanner.Token == Symbols.ListSeparator)
            {
                Scanner.Next();
                ReadVar();
            }
            MatchString(Symbols.CloseParenthesis.ToString());
        }

        //{ Process a Write Statement }
        void DoWrite()
        {
            Tree.AddNode("چاپ خروجی");
            Scanner.Next();
            MatchString(Symbols.OpenParenthesis.ToString());
            String[] Options = { "StringExpression", "BoolExpression", "MathExpression(DataType.Integer)", "MathExpression(DataType.Real)" };
            String Best = ParseBest(Options);
            switch (Best)
            {
                case "StringExpression":
                    Translator.WriteString();
                    break;
                case "MathExpression(DataType.Integer)":
                    Translator.WriteInteger();
                    break;
                case "MathExpression(DataType.Real)":
                    Translator.WriteReal();
                    break;
                case "BoolExpression":
                    Translator.WriteBoolean();
                    break;
                default:
                    throw new Exception("This option isn't in the case!");
            }
            /*while (Scanner.Token == Symbols.ListSeparator)
            {
                Scanner.Next();
                Expression(DataType.Integer);
                Translator.WriteInteger();
            }*/
            MatchString(Symbols.CloseParenthesis.ToString());
            Tree.Up();
        }

        // { Parse and Translate a Procedure Declaration }
        void DoProc()
        {
            //
            while (Scanner.Token == Tokens.Procedure)
            {
                Scanner.GetName();
                String Name = Scanner.Value;
                Tree.AddNode("روش " + Name);
                // Scanner.Next();
                //MatchString(Keywords.BeginBlock);
                //Fin;
                if (InTable(Scanner.Value)) Duplicate(Scanner.Value);
                AddEntry(Scanner.Value, SymbolType.Procedure);
                //PostLabel(Value);
                Translator.ProcProlog(Name);
                FormalList();
                BeginBlock();
                //Store("حاصل");
                Translator.ProcEpilog(Name, ParamSizes);
                AddProcedure(Name, Params);
                Params.Clear();
                ParamSizes = 0;
                LocalSizes = 0;
                Tree.Up();
            }

        }

        void AddProcedure(String N, Dictionary<String, Parameter> Params)
        {
            Procedures[N] = Params.Where(param => param.Value.Type != ParamType.Local).ToArray();
        }

        void FormalList()
        {
            Scanner.Next();
            MatchString(Symbols.OpenParenthesis.ToString());
            if (Scanner.Token != Symbols.CloseParenthesis)
            {
                InputVars();
                OutputVars();
            }
            MatchString(Symbols.CloseParenthesis.ToString());
            //Translator.LoadParamValues(Params);
        }

        void InputVars()
        {
            //Scanner.Next();
            if (Scanner.Token == Tokens.InputVar)
            {
                MatchString(Keywords.Input);
                MatchString(Keywords.BeginBlock);
                while (Scanner.Token != '؛')
                {
                    FormalParam(ParamType.ByVal);
                    while (Scanner.Token == Symbols.ListSeparator)
                    {
                        MatchString(Symbols.ListSeparator.ToString());
                        FormalParam(ParamType.ByVal);
                    }
                }
                MatchString("؛");
            }
        }

        void OutputVars()
        {
            //Scanner.Next();
            if (Scanner.Token == Tokens.OutputVar)
            {
                MatchString(Keywords.InOut);
                MatchString(Keywords.BeginBlock);
                while (Scanner.Token != '؛')
                {
                    FormalParam(ParamType.ByRef);
                    while (Scanner.Token == Symbols.ListSeparator)
                    {
                        MatchString(Symbols.ListSeparator.ToString());
                        FormalParam(ParamType.ByRef);
                    }
                }
                MatchString("؛");
            }
        }//{ Process a Formal Parameter }

        void FormalParam(ParamType PType)
        {
            if (Scanner.Token == Tokens.Identifier)
            {
                String Name = Scanner.Value;
                DataType Dtype = DataType.Integer;
                Scanner.Next();
                if (Scanner.Token == Tokens.VarType)
                {
                    Dtype = Scanner.GetDataType();

                }
                AddParam(Name, PType, Dtype);

                //Translator.tmpAddParam(Scanner.Value);

            }
            else
            {
                Abort(Scanner.Value + " یک ورودی یا خروجی مناسب نیست");
            }
        }

        //{ Process an Actual Parameter }
        void Param(ParamType PType, DataType DType)
        {
            if (PType == ParamType.ByRef)
            {
                //Scanner.GetName();
                String P = Scanner.Value;
                CheckTables(P);
                DataType Dt = DataTypeOf(P);
                if (Dt != DType) Expected("متغیر ازنوع " + Farsi.Translate(DType.ToString()), Farsi.Translate(Dt.ToString()));
                Translator.GetRefrence(P);
                Translator.Push();
                Tree.AddNode("ارجاع به "+P);
                Tree.Up();
                Scanner.Next();
            }
            else
            {
                /*String[] options = { "BoolExpression", "MathExpression(DataType.Real)", "MathExpression(DataType.Integer)" };
                ParseBest(options);*/
                Expression(DType);
                switch (DType)
                {
                    case DataType.String:
                        Translator.PassStringByVal();
                        break;
                    case DataType.Boolean:
                    case DataType.Integer:
                        Translator.Push();
                        break;
                    case DataType.Real:
                        Translator.FPush();
                        break;
                }
            }

        }

        //{ Process the Parameter List for a Procedure  Call }
        void ParamList(String N)
        {
            int n = 0;
            String[] tmp = new String[100];
            Translator.Mode = Translate.OutputMode.StandBy;
            MatchString(Symbols.OpenParenthesis.ToString());
            if (Scanner.Token != Symbols.CloseParenthesis)
            {
                if (n > Procedures[N].Count()) Abort("تعداد پارامترها بیش از حد نیاز است");
                Param(Procedures[N][n].Value.Type, Procedures[N][n].Value.DType);
                tmp[n] = Translator.Buffer;
                n++;
                while (Scanner.Token == Symbols.ListSeparator)
                {
                    MatchString(Symbols.ListSeparator.ToString());
                    if (n >= Procedures[N].Count()) Abort("تعداد پارامترها بیش از حد نیاز است");
                    Param(Procedures[N][n].Value.Type, Procedures[N][n].Value.DType);
                    tmp[n] = Translator.Buffer;
                    n++;
                }
            }
            if (n < Procedures[N].Count()) Abort("تعداد پارامترها کمتر از حد نیاز است");
            MatchString(Symbols.CloseParenthesis.ToString());
            Translator.Mode = Translate.OutputMode.RealTime;
            for (int i = n; i >= 0; i--)
            {
                Translator.DirectEmit(tmp[i]);
            }
            /*
             * the constant 2 is default size of our parameters
             * we assumed that every parameter size is 2 bytes:
             */
            /**
             * We don't need ParamList as a function,since we did stack restoration in Translate.ProcEpilog()
             * */
            //return n * 2;
        }

        bool IsParam(String N)
        {
            return Params.ContainsKey(N);
        }

        //{ Add a New Parameter to Table }
        void AddParam(String Name, ParamType PType, DataType DType)
        {
            if (IsParam(Name)) Duplicate(Name);
            Params[Name] = new Parameter(Params.Count + 1, PType, DType);
            switch (PType)
            {
                case ParamType.Local:
                    LocalSizes += Params[Name].Size();
                    Params[Name].Offset = -LocalSizes;
                    break;
                case ParamType.ByVal:
                case ParamType.ByRef:
                    //Procedures.Last().
                    Params[Name].Offset = ParamSizes;
                    ParamSizes += Params[Name].Size();
                    break;
            }
        }

        int CalculateOffset(Parameter P)
        {
            int offset = 0;

            for (int i = 0; i < Params.Count; i++)
            {

                if (Params.ElementAt(i).Value == P) return offset;
                offset += Params.ElementAt(i).Value.Size();
            }
            return offset;
        }

        //{ Load a Parameter to the Pr imary Register }
        void LoadParam(String N, DataType DType)
        {
            //int Offset;
            //Offset = 8 + 4 * (Params.Count - Params[N].Seq);

            Translator.LoadParam(Params[N], DType);
            //if (Params[N].Type == ParamType.ByRef) Translator.Derefrence();
        }

        //{ Store a Parameter from the Pr imary Register }
        void StoreParam(String N)
        {
            //int Offset = 0;
            //Offset = 8 + 2 * (Params.Count - Params[N]);
            Translator.StoreParam(Params[N]);
        }

        //{ Process a Procedure Call }
        void CallProc(String Name)
        {
            Tree.AddNode("فراخوانی تابع " + Name);
            //CheckTables(Name);
            Scanner.Next();
            ParamList(Name);
            Translator.Call(Name);
            //Translator.CleanStack(n);
            Tree.Up();
        }

        void LocDecls()
        {
            if (Scanner.Token == 'L')
            {
                MatchString(Keywords.Local);
                MatchString(Keywords.BeginBlock);

                while (Scanner.Token != Tokens.EndBlock)
                {
                    String[] Locals = new String[100];
                    int localCount = 0;
                    DataType dt = DataType.Integer;
                    //Scanner.Next();
                    if (Scanner.Token != Tokens.Identifier) Expected("شناسه", Farsi.Translate(Scanner.Token.ToString()));
                    Locals[localCount++] = Scanner.Value;
                    Scanner.Next();
                    while (Scanner.Value == "و")
                    {
                        Scanner.Next();
                        if (Scanner.Token != Tokens.Identifier) Expected("شناسه", Farsi.Translate(Scanner.Token.ToString()));
                        Locals[localCount++] = Scanner.Value;
                        Scanner.Next();
                    }
                    if (Scanner.Token == Tokens.VarType)
                    {
                        dt = Scanner.GetDataType();
                    }
                    for (int i = 0; i < localCount; i++)
                    {
                        AddParam(Locals[i], ParamType.Local, dt);
                        if (dt == DataType.String) Translator.LocalStrParam(Params[Locals[i]].Offset);
                    }
                    Semi();

                }
                MatchString(Tokens.EndBlock.ToString());
                Translator.AllocateRoom(LocalSizes);
            }
        }

        /*void LocAlloc()
        {
            Scanner.Next();
            if (Scanner.Token != Tokens.Identifier) Expected("نام متغیر", Scanner.Token.ToString());
            AddParam(Scanner.Value, ParamType.Local, DataType.Integer);
            Scanner.Next();
        }*/
        // { Parse and Translate a Begin-Block }
        void BeginBlock()
        {
            //Next();
            MatchString(Keywords.BeginBlock);
            LocDecls();
            Block();
            MatchString(Keywords.EndProc);
        }

        void Block(/*String L*/)
        {
            Char[] a = { 'e', 'l', '؛' };
            //Scan();
            while (Array.IndexOf(a, Scanner.Token) < 0)
            {
                switch (Scanner.Token)
                {
                    case Tokens.If:
                        DoIf();
                        break;
                    case Tokens.While:
                        DoWhile();
                        break;
                    case Tokens.Write:
                        DoWrite();
                        break;
                    case Tokens.Read:
                        DoRead();
                        break;
                    case Tokens.Var:
                        DoVar();
                        break;
                    /*case 'p':
DoLoop();
break;
case 'r':
DoRepeat();
break;
*/
                    case Tokens.For:
                        DoFor();
                        break;

                    /*case 'd':
                    Dodo();
                    break;
                    case 'b':
                    DoBreak(L);
                    break;
                    */
                    default://case Tokens.Identifier:
                        AssignOrProc();
                        break;
                    /*
                        Abort(Token + " ناخواسته است.");
                        break;*/

                }
                Semi();
                //Scan();
            }

        }

        //{ Allocate Storage for a Variable }
        void Alloc(String Name, DataType DType)
        {
            CheckDup(Name);
            AddEntry(Name, SymbolType.Var, DType);
            Translator.Allocate(Name, DType, "0");
            //Scanner.Next();
        }

        //{ Parse and Translate Global Declarations }
        //I keep this method to match with pdf even only in name
        void TopDecls()
        {
            Tree.AddNode("تعاریف");
            while (Scanner.Token != '.' & Scanner.Token != '@')
            {
                switch (Scanner.Token)
                {
                    case Tokens.Var:
                        DoVar();
                        break;
                    case Tokens.Procedure:
                        DoProc();
                        break;
                    case Tokens.Block:
                        DoMain();
                        break;
                    default:
                        Expected("تعریف متغیر یا روش یا برنامه اصلی", /*Farsi.Translate(Scanner.Token.ToString())+*/"\"" +Scanner.Value+"\"");
                        break;
                }
            }
            Tree.Up();
        }

        void DoVar()
        {
            String[] vars = new String[100];
            int varCount = 0;
            DataType dt = DataType.Integer;
            Tree.AddNode("متغیر");
            //Scan();
            //MatchString("تعاریف");
            if (Scanner.Token == Tokens.Var)
            {
                Scanner.Next();
                if (Scanner.Token != Tokens.Identifier) Expected("نام متغیر", Farsi.Translate(Scanner.Token.ToString()));
                vars[varCount++] = Scanner.Value;
                Scanner.Next();
                while (Scanner.Value == "و")
                {
                    Scanner.Next();
                    if (Scanner.Token != Tokens.Identifier) Expected("نام متغیر", Farsi.Translate(Scanner.Token.ToString()));
                    vars[varCount++] = Scanner.Value;
                    Scanner.Next();
                }
                if (Scanner.Token == Tokens.VarType)
                {
                    dt = Scanner.GetDataType();
                }
                for (int i = 0; i < varCount; i++)
                {
                    Alloc(vars[i], dt);
                }
            }
            //Alloc();
            Semi();
            Tree.Up();
        }

        //{ Parse and Translate a Main Program }
        void DoMain()
        {
            Tree.AddNode("برنامه اصلی");
            MatchString(Keywords.Program);
            if (InTable(Scanner.Value)) Duplicate(Scanner.Value);
            int lukap = Lookup(SType, SymbolType.Main);
            if (lukap > -1) Abort("برنامه ی اصلی قبلا با نام \" " + ST[lukap] + "\" تعریف شده است");
            AddEntry(Scanner.Value, SymbolType.Main);
            Scanner.Next();
            MatchString(Keywords.Begin);
            Translator.Prolog();
            Block();
            MatchString(Keywords.End);
            Translator.Epilog();
            Tree.Up();
        }

        //{  Parse and Translate a Program }
        void Prog()
        {
            try
            {
                MatchString("به");
                MatchString("نام");
                MatchString("خدا");
            }
            catch (Exception e) {
                Abort("هر کاری که با نام و یاد خدا انجام نشود، بی سرانجام است");
            }
            Semi();
            Translator.Header();
            TopDecls();
            MatchString(".");
            Scanner.Next();
            if (Scanner.Look != '@')
            {
                Abort("متن اضافی پس از پایان برنامه");
            }
        }

        public String Parse(String What/*,ref String Result*/)
        {
            try
            {
                switch (What)
                {
                    case "Program":
                        Prog();
                        break;
                    case "BoolExpression":
                        BoolExpression();
                        break;
                    case "MathExpression(DataType.Integer)":
                        MathExpression(DataType.Integer);
                        break;
                    case "MathExpression(DataType.Real)":
                        MathExpression(DataType.Real);
                        break;
                    case "StringExpression":
                        StringExpression();
                        break;
                    case "RelationString":
                        RelationString();
                        break;
                    case "RelationReal":
                        RelationReal();
                        break;
                    case "RelationInteger":
                        RelationInteger();
                        break;
                    case "RelationBoolean":
                        RelationBoolean();
                        break;

                }

                return Translator.GetText();
                //return true;
                //String Token = "";
            }
            catch (Exception e1)
            {
                int l = (Scanner.Look == CR) ? Scanner.line - 1 : Scanner.line;
                throw new Exception(e1.Message + " خط " + (l).ToString());
            }
        }

        public int TryParse(String What, ref String Errors)
        {
            Parser tmp = this.Clone();
            LexicalScan tmps = this.Scanner.Clone();
            tmp.Scanner = tmps;
            Translate.OutputMode tmpMode = this.Translator.Mode;
            tmp.Translator.Mode = Translate.OutputMode.Off;
            try
            {
                tmp.Parse(What);
                this.Translator.Mode = tmpMode;
                return tmp.Scanner.pos - this.Scanner.pos;
            }
            catch (Exception ex)
            {

                //this.Copy(tmp);
                //this.Scanner = tmps;
                //Errors += this.Errors;
                this.Translator.Mode = tmpMode;
                Errors += ex.Message;
                return -1;
            }
        }

        public String ParseBest(String[] Whats)
        {
            int max = -1;
            int res = -1;
            String tmp = "";
            String best = Whats[0];
            foreach (String Line in Whats)
            {
                res = TryParse(Line, ref tmp);
                if (max < res)
                {
                    max = res;
                    best = Line;
                }
            }
            switch (best)
            {
                case "Program":
                    Prog();
                    break;
                case "BoolExpression":
                    BoolExpression();
                    break;
                case "MathExpression(DataType.Integer)":
                    MathExpression(DataType.Integer);
                    break;
                case "MathExpression(DataType.Real)":
                    MathExpression(DataType.Real);
                    break;
                case "StringExpression":
                    StringExpression();
                    break;
                case "RelationString":
                    RelationString();
                    break;
                case "RelationReal":
                    RelationReal();
                    break;
                case "RelationInteger":
                    RelationInteger();
                    break;
                case "RelationBoolean":
                    RelationBoolean();
                    break;
                default:
                    throw new Exception("This option isn't in the case!");
            }
            return best;
        }

        public Parser Clone()
        {
            return (Parser)this.MemberwiseClone();
        }

        public void Copy(Parser Cop)
        {
            this.LocalSizes = Cop.LocalSizes;
            this.Tree = Cop.Tree;
            this.Errors = Cop.Errors;
            this.NEntry = Cop.NEntry;
            this.Params = Cop.Params;
            this.ParamSizes = Cop.ParamSizes;
            this.Procedures = Cop.Procedures;
            //this.Scanner = Cop.Scanner;
            this.ST = Cop.ST;
            this.SType = Cop.SType;
            this.Translator = Cop.Translator;
            this.Tree = Cop.Tree;
        }

        /*
         * checks whether type1 is compatible with type2 or not?
         * this method and some others related to data types (if added later)
         * should take place in a seprate module
         **/
        public Boolean AreCompatible(DataType type1, DataType type2)
        {
            if (type1 == DataType.Integer && type2 == DataType.Real)
                return true;
            else
                return type1 == type2;
        }


    }
}
