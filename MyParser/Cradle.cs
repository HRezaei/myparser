﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyParser
{
    class Cradle
    {
        //{ Constant Declarations }
        Char TAB = '\t';
        Char CR = '\n';

        //{ Variable Declarations }
        //Nullable<Char> Look;             // { Lookahead Character }
        Char Look;
        int pos = 0;

        //{ Read New Character From Input Stream }
        void GetChar()
        {
            if (pos < richTextBoxCode.Text.Length)
            {
                Look = richTextBoxCode.Text[pos];
                pos++;
            }
            else
            {
                Look = '@';
            }
        }

        //{ Report an Error }
        void Error(string s)
        {
            richTextBoxMsg.AppendText("\n");
            richTextBoxMsg.AppendText("خطا: " + s + ".");
        }

        //{ Report Error and Halt }
        void Abort(string s)
        {
            Error(s);
            throw new Exception("error: " + s);
        }

        //{ Report What Was Expected }
        void Expected(string s)
        {
            Abort(s + " نیاز است");
        }

        //{ Recognize an Alpha Character }
        bool IsAlpha(char c)
        {
            return char.IsLetter(c);
        }


        //{ Recognize a Decimal Digit }
        bool IsDigit(char c)
        {
            return char.IsDigit(c);
        }

        bool IsAlNum(char c)
        {
            return IsAlpha(c) || IsDigit(c);
        }

        bool IsAddop(char c)
        {
            Char[] a = { '+', '-' };
            return (Array.IndexOf(a, c) >= 0);
        }

        bool IsWhite(char c)
        {
            return char.IsWhiteSpace(c);
        }

        void SkipWhite()
        {
            while (IsWhite(Look)) GetChar();
        }

        //{ Match a Specific Input Character }
        void Match(char x)
        {
            if (Look != x)
                Expected("'" + x + "'");
            else
            {
                GetChar();
                SkipWhite();
            }
        }



        //{ Get an Identifier }
        Char GetName()
        {
            String Token = "";
            if (!IsAlpha(Look)) Expected("شناسه");

            while (IsAlNum(Look))
            {
                Token = Token + char.ToUpper(Look);
                GetChar();
            }
            SkipWhite();
            return Token;
        }

        //{ Get a Number }
        Char GetNum()
        {
            String Value = "";
            if (!IsDigit(Look)) Expected("عدد صحیح");
            while (IsDigit(Look))
            {
                Value = Value + Look;
                GetChar();
            }
            SkipWhite();
            return Value;
        }

        //{ Output a String with Tab }
        void Emit(string s)
        {
            richTextBoxResult.AppendText(TAB + s);
        }

        //{ Output a String with Tab and CRLF }
        void EmitLn(string s)
        {
            Emit(s);
            richTextBoxResult.AppendText("\n");
        }

        void Ident()
        {
            String Name = GetName();
            if (Look == '(')
            {
                Match('(');
                Match(')');
                EmitLn("BSR " + Name);
            }
            else
                EmitLn("MOVE " + Name + "(PC),D0");
        }

        void Factor()
        {
            if (Look == '(')
            {
                Match('(');
                Expression();
                Match(')');
            }
            else if (IsAlpha(Look))
            {
                Ident();
            }
            else

                EmitLn("MOVE #" + GetNum() + " , D0 ");
        }

        void Multiply()
        {
            Match('*');
            Factor();
            EmitLn("MULS (SP)+,D0");
        }

        void Divide()
        {
            Match('/');
            Factor();
            EmitLn("MOVE (SP)+,D1");
            EmitLn("DIVS D1,D0");
        }

         void Term()
        {
            Char[] a = { '*', '/' };
            Factor();
            while (Array.IndexOf(a, Look) >= 0)
            {
                EmitLn("MOVE D0,-(SP)");
                switch (Look)
                {
                    case '*':
                        {
                            Multiply();
                            break;
                        }
                    case '/':
                        {
                            Divide();
                            break;
                        }
                    default: Expected("عملگر");
                        break;
                }
            }

        }

void Add()
        {
            Match('+');
            Term();
            EmitLn("ADD (SP)+,D0");
        }

        //{ Recognize and Translate a Subtract }
        void Subtract()
        {
            Match('-');
            Term();
            EmitLn("SUB (SP)+,D0");
        }

        void Expression()
        {
            //Char[] a = { '+', '-' };
            if (IsAddop(Look))
            {
                EmitLn("CLR D0");
            }
            else
            {
                Term();
            }

            while (IsAddop(Look))
            {
                EmitLn("MOVE D0,-(SP)");
                switch (Look)
                {
                    case '+':
                        {
                            Add();
                            break;
                        }
                    case '-':
                        {
                            Subtract();
                            break;
                        }
                    default: Expected("Addop");
                        break;
                }
            }

        }

void Assignment()
        {
            String Name = GetName();
            Match('=');
            Expression();
            EmitLn("LEA " + Name + "(PC),A0");
            EmitLn("MOVE D0,(A0)");
        }

//{ Initialize }
        void Init()
        {
            pos = 0;
            richTextBoxMsg.Text = richTextBoxResult.Text = "";
            GetChar();
            SkipWhite();
        }









        
        
    }
}
