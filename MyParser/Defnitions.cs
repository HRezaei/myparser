﻿namespace MyParser
{
    public enum ParamType { Local, ByVal, ByRef };

    public enum DataType { Integer, Real, Boolean, String };

    public struct SymbolType
    {
        public const char Main = 'M';
        public const char Var = 'v';
        public const char Procedure = 'p';
        public const char FormalParam = 'f';
    }

    public class Parameter
    {
        /*
         * I commented it because I love using dictionary in this enviroment!
         */
        //String Name;
        //Size of parameter or local variable in bytes:
        //public int Size ;
        //order of parameter in the procedure declaration or signature:
        public int Offset;
        /*
         * We use this struct to manage both parameters and local variables,
         * This field added to distinguish between them since there is some diffrences in some impelemention aspects
         */
        public ParamType Type = ParamType.ByVal;
        public DataType DType = DataType.Integer;

        public Parameter(int offset, ParamType ptype, DataType dt)
        {
            Offset = offset;
            Type = ptype;
            DType = dt;

        }
        public int Size()
        {
            switch (Type)
            {
                case ParamType.ByRef:
                    return Translate.AdressSize;
                default:
                    return Translate.GetDataTypeSize(DType);

            }

        }   /*
        public int Offset(Dictionary<String,Parameter> Params) {
            int offset = 0;
            for(int i=0;i<Params.Count;i++) { 
                
            }
            return offset;
        }
        */

    }

    public struct Tokens
    {
        public const char Identifier = 'x';
        public const char RealConst = '%';
        public const char IntegerConst = '#';
        public const char StringConst = '$';
        public const char BooleanConst = '^';
        public const char OutputVar = 'O';
        public const char InputVar = 'I';
        public const char Procedure = 'P';
        public const char Step = 's';
        public const char If = 'i';
        public const char Else = 'l';
        public const char While = 'w';
        public const char Read = 'R';
        public const char Write = 'W';
        public const char Var = 'v';
        public const char Block = 'B';
        public const char For = 'f';
        public const char Local = 'L';
        public const char VarType = 'T';
        public const char EndBlock = '؛';
        //public const char Operator = 'o';
    }

    public struct Symbols
    {
        public const char EqualSign = '=';
        public const char NegateOp = '!';
        public const char AndOp = '&';
        public const char OrOp = '|';
        public const char XorOp = '~';
        public const char DivideOp = '÷';
        public const char MultiplyOp = '×';
        public const char AddOp = '+';
        public const char SubtractOp = '-';
        public const char FPSign = '/';
        public const char AssignOp = '→';
        public const char ListSeparator = '،';
        public const char OpenParenthesis = '(';
        public const char CloseParenthesis = ')';
        public const char OpenComments = '{';
        public const char CloseComments = '}';

    }

    public struct Keywords
    {
        public const string Program = "برنامه";
        public const string Var = "متغیر";
        public const string VarType = "ازنوع";
        public const string Begin = "شروع";
        public const string End = "پایان";

        public const string Procedure = "روش";
        public const string Input = "ورودی";
        public const string InOut = "وروجی";
        public const string BeginBlock = ":";
        public const string Local = "داخلی";
        public const string EndProc = "بازگشت";

        public const string If = "اگر";
        public const string Bud = "بود";
        public const string Else = "وگرنه";
        public const string EndIf = "؛";

        public const string While = "تاوقتی";
        public const string Hast = "هست";
        public const string EndWhile = "؛";

        public const string For = "برای";
        public const string Az = "از";
        public const string Ta = "تا";
        public const string Step = "باگام";
        public const string EndFor = "؛";

        public const string Read = "بخوان";
        public const string Write = "بنویس";

        public const string True = "درست";
        public const string False = "نادرست";

        /*
         * 
            public const String EndIf = "پایانگر";
            public const String EndWhile = "پایان_تاوقتی";
         
         */

    }

    public class Farsi
    {

        public static string Translate(string eng)
        {
            System.Collections.Generic.Dictionary<string, string> Words = new System.Collections.Generic.Dictionary<string, string>()
                {
	                {Tokens.Identifier.ToString(),"شناسه"},
	                {Tokens.IntegerConst.ToString(), "عددصحیح"},
                    {Tokens.RealConst.ToString(),"عدد حقیقی"},
                    {Tokens.StringConst.ToString(), "رشته کاراکتری"},
                    {Tokens.Procedure.ToString(),"روش"},
                    {Tokens.VarType.ToString(),"نوع داده"},
                    {Tokens.BooleanConst.ToString(),"ثابت منطقی"},
                    //{Tokens.Operator.ToString(),"عملگر"},
                    {"Real","ممیز شناور"},
                    {"Integer","صحیح"},
                    {"String","رشته کاراکتری"},
                    {"Boolean","منطقی"}
	            };
            if (Words.ContainsKey(eng))
                return Words[eng];
            else
                return eng;
        }
        /*
        public static string Translate(DataType dtype) {
            switch (dtype) { 
                case DataType.Real:
                    return"ممیز شناور";
                case DataType.Integer:
                    return "صحیح";
                case DataType.String:
                    return "رشته";
                case DataType.Boolean:
                    return "منطقی";
                default:
                    return dtype.ToString();
            }
        }*/
    }

}