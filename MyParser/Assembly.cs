﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyParser
{
    class Assembler
    {
        public static void Assemble(String[] Codes,String FileName) {
            System.IO.File.WriteAllLines(FileName + ".asm", Codes);
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "assmbl.bat";
            startInfo.Arguments = /*"D:\\masm32\bin\\ml /c /Zd /coff " +*/ "\"" + FileName + ".asm\" \"" + FileName + ".obj\"" /*+ ".asm"*/;
            process.StartInfo = startInfo;
            process.Start();
            //string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
        }
    }
}
