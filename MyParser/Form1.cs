﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyParser
{
    public partial class Form1 : Form
    {

        private Boolean Saved = false;
        private String FPath;
        private String PName = "barname";
        private String FName;
        private Parser ParParser;
        public Form1()
        {
            InitializeComponent();
            ParParser = new Parser("");
        }

        private void Save()
        {
            if (!Saved) saveFileDialog1.ShowDialog();
            if (FPath != null)
            {
                System.IO.File.WriteAllLines(FName + ".par", richTextBoxCode.Lines);
            }
            Saved = true;
            this.Text = PName;
        }

        private void Assemble()
        {
            if (!Saved) Save();
            Assembler.Assemble(richTextBoxResult.Lines, FName);

        }


        private void Link()
        {
            Linker.Link(FName);
        }

        private void Run()
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //startInfo.FileName = "run.bat";
            //startInfo.Arguments = "\"" + FName + ".exe\" \"" + FName + ".txt\"";
            startInfo.FileName = FName + ".exe";
            //startInfo.Arguments = "\"" + FName + ".exe\" \"" + FName + ".txt\"";
            startInfo.Arguments = ">result.txt";
            //startInfo.RedirectStandardInput = true;
            //startInfo.RedirectStandardOutput = true;
            //startInfo.UseShellExecute = false;
            process.StartInfo = startInfo;
            process.Start();
            //string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
        }

        //{ Report an Error }
        public void Error(string s)
        {
            richTextBoxMsg.AppendText("\n");
            richTextBoxMsg.AppendText("خطا: " + s + ".");
        }

        public String GetCode()
        {
            return richTextBoxCode.Text;
        }

        //{ Initialize }
        void Init()
        {
            ParParser = new Parser(richTextBoxCode.Text);
            richTextBoxMsg.Text = richTextBoxResult.Text = "";
            toolStripStatusLabel1.Text = "";
            }

        

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                Init();
                richTextBoxResult.Text = ParParser.Parse("Program");
                richTextBoxMsg.Text = ParParser.Errors;
                
            }
            catch (Exception e2)
            {
                richTextBoxMsg.Text = e2.Message;
            }
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            String path = saveFileDialog1.FileName;
            int len = path.LastIndexOf('\\');
            FPath = path.Substring(0, len + 1);
            PName = path.Substring(len + 1, path.LastIndexOf('.') - len - 1);

            FName = FPath + PName;
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Assemble();
        }

        private void richTextBoxCode_TextChanged(object sender, EventArgs e)
        {
            if (Saved)
            {
                Saved = false;
                this.Text += "*";
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            Link();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            Run();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            try
            {
                Assemble();
                Link();
                Run();
            }
            catch (Exception ex)
            {
                toolStripStatusLabel1.Text = ex.Message;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String sum = "";
            Byte[] asciiBytes = Encoding.Unicode.GetBytes(richTextBoxMsg.Text);
            for (int i = 0; i < asciiBytes.Length; i += 2)
            //foreach (byte b in asciiBytes)
            {
                sum = asciiBytes[i + 1].ToString("X") + asciiBytes[i + 0].ToString("X");// +asciiBytes[i + 1] + asciiBytes[i + 2] + asciiBytes[i + 3];
                MessageBox.Show("mov [tmp+" + i.ToString() + "]," + sum + "h");
                sum = "";
            }

        }


        private void copyToolStripButton_Click(object sender, EventArgs e)
        {
            treeView1.Nodes.Add((System.Windows.Forms.TreeNode)ParParser.Tree.CurNode);
            treeView1.ExpandAll();
        }

        private void richTextBoxCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '*') e.KeyChar = '→';
            /* 
             if (e.KeyChar < '9' && e.KeyChar > '0')
             {
                
                 Byte[] bytes = { 217, 160 };
                 bytes[1] = Convert.ToByte(160 + Convert.ToInt32(Char.GetNumericValue(e.KeyChar)));
                 System.Text.Decoder utf8Decoder = System.Text.UTF8Encoding.UTF8.GetDecoder();

                 Char[] cTransChar = { ' ' };
                 utf8Decoder.GetChars(bytes, 0, 2, cTransChar, 0);
                  e.KeyChar = cTransChar[0];
             */





            /*var number = e.KeyChar;
            var culture = System.Globalization.CultureInfo.CreateSpecificCulture("fa-Ir");
            e.KeyChar = String.Format(culture, "{0}", number)[0];
            //MessageBox.Show(text);}

            //System.Globalization.CultureInfo.c;
* 
        }
*/
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            richTextBoxCode.Text=System.IO.File.ReadAllText(openFileDialog1.FileName);
        }

        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

    }
}
